//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef aevaAboutDialogReaction_h
#define aevaAboutDialogReaction_h

#include "pqReaction.h"

/**
* @ingroup Reactions
* aevaAboutDialogReaction used to show the standard about dialog for the
* application.
*/
class aevaAboutDialogReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  aevaAboutDialogReaction(QAction* parent);

  /**
  * Shows the about dialog for the application.
  */
  static void showAboutDialog();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { aevaAboutDialogReaction::showAboutDialog(); }

private:
  Q_DISABLE_COPY(aevaAboutDialogReaction)
};

#endif
